/*******************
    D Rupinder
***********************/


#include<bits/stdc++.h>

#define lld long long int
#define FOR(i,a,b) for(i= a ; i < b ; ++i)
#define rep(i,n) FOR(i,0,n)
#define repn(i,n) FOR(i,1,n+1)
#define all(x) x.begin(),x.end()
#define LET(x,a) __typeof(a) x(a)
#define IFOR(i,a,b) for(LET(i,a);i!=(b);++i)
#define EACH(it,v) IFOR(it,v.begin(),v.end())
#define pb push_back
#define sz size()
#define pii pair<int, int>
#define pll pair <lld ,lld>
#define mp make_pair
#define fill(x,v) memset(x,v,sizeof(x))
#define scan(v,n) vector<int> v(n);rep(i,n)cin>>v[i];
#define vi vector<int>
#define MOD 1e9 + 7
#define ff first()
#define ss second()

using namespace std;
lld modpow(lld a,lld n,lld temp){lld res=1,y=a;while(n>0){if(n&1)res=(res*y)%temp;y=(y*y)%temp;n/=2;}return res%temp;} 

int main()
{
printf("        .file   \"a.cpp\"\n        .section        .rodata\n.LC0:\n        .string \"%%d\"\n        .text\n        .globl  main\n        .type   main, @function\nmain:\n.LFB0:\n        .cfi_startproc\n        pushq   %%rbp\n        .cfi_def_cfa_offset 16\n        .cfi_offset 6, -16\n        movq    %%rsp, %%rbp\n        .cfi_def_cfa_register 6\n        subq    $16, %%rsp\n        movl    $4, -8(%%rbp)\n        movl    $5, -4(%%rbp)\n        movl    -4(%%rbp), %%eax\n        movl    -8(%%rbp), %%edx\n        addl    %%edx, %%eax\n        movl    %%eax, %%esi\n        movl    $.LC0, %%edi\n        movl    $0, %%eax\n        call    printf\n        movl    $0, %%eax\n        leave\n        .cfi_def_cfa 7, 8\n        ret \n        .cfi_endproc\n.LFE0:\n        .size   main, .-main \n        .ident  \"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4\"\n        .section        .note.GNU-stack,\"\",@progbits\n");	
return 0;
}
